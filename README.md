# Ansible Role: PHP-MongoDB

Installs PHP [MongoDB](https://www.mongodb.com/) support on Linux.

## Requirements

None.

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    php_enablerepo: ""

(RedHat/CentOS only) If you have enabled any additional repositories (might I suggest geerlingguy.repo-epel or geerlingguy.repo-remi), those repositories can be listed under this variable (e.g. `remi,epel`). This can allow you to install later versions of PHP packages.

    php_mongodb_package: php-mongodb # RedHat
    php_mongodb_package: php5-mongodb # Debian

The PHP MongoDB package to install via apt/yum. This should only be overridden if you need to install a unique/special package for MongoDB support, as in the case of using software collections on Enterprise Linux.

## Dependencies

  - geerlingguy.php

## Example Playbook

    - hosts: webservers
      roles:
        - { role: daffie.php-mongodb }

## License

MIT / BSD

## Author Information

This role was created in 2018 by [Daffie](http://www.gitlab.com/daffie).
